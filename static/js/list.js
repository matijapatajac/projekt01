var app = angular.module('projekt');

app.controller('ListController'
  , function($rootScope, $scope, $http){
      var self = this;
      this.users = [];
      getUsers();

      this.log = function(user){
        console.log(user);
      }

      function getUsers(){
        var headers = {'X-Auth-Token' : $rootScope.token};
        var request = {method:'GET', url:'/api/users', headers: headers};

        console.log("SALJEM: ", request);

      $http(request)
      .then(function successCallback(response){
      console.log("users: ", response);
      self.users = response.data;  
      })
      .catch(function errorCallback(response){
      console.log("getUsers ERROR", response);
    });
  }
});