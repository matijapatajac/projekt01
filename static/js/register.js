var app = angular.module('projekt');

app.controller('RegisterController'
  , function($rootScope, $scope, $http){
  
    var self = this;
  this.email = "prazno";
  this.pass = "prazno";
  $rootScope.registerSuccess = false;

  this.send = function(email, pass){
    self.email = email;
    self.pass = pass;

    var data = {email:email, pass:pass};
    $http({
      data:data,
      method:'POST',
      url:'/register'
    }).then(function successCallback(response){
      console.log("response", response);
      $rootScope.registerSuccess = true;
    }, function errorCallback(response){
      console.log("GRESKA");
      $rootScope.registerSuccess = false;
    });

  };

});