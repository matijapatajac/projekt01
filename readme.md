#**Evaluator**

##Brief Description
A program which analyzes given tasks on an established set of rules and returns results of the evaluation.

##Authorization
Gaining access to directories.

*Route*
```
POST /auth/
```
*Request body*
```
{"email": emailInfo, "password": passwordInfo }
```
*Response body:*
```
{"token": "k59dl2to8d4d0nflke2ck7ns50f2wh7p"}
```
## Exams methods
 - **Obtaining all exams**
 
Downloads all exams.

*Route:*
```
GET /auth/exams/
```
*Request body:*
```
{
	"exam-list": []
}
```
*Response body:*
```
{
	"exam-list": [
		{ "grade-test-list": [...]},
		{ "grade-test-list": [...]},
		{ "grade-test-list": [...]},
		{ "grade-test-list": [...]},
		{ "grade-test-list": [...]},
	]
}
//for contents of grade-test-list, see below
```
Note: recommended only for administrator

----------


 - **Obtaining all exams within a grade**

Downloads all exams within the desired grade.
*Route:*
```
GET /auth/exams/grade/
```
*Request body:*
```
{
	"grade-test-list": []
}
```
*Response body:*
```
{
	"grade-test-list": [
		{ "test-tasks": [...]},
		{ "test-tasks": [...]},
		{ "test-tasks": [...]},
		{ "test-tasks": [...]},
		{ "test-tasks": [...]},
	]
}
//for contents of test-tasks, see below
```
Note: recommended only for administrator

----------

 - **Obtaining specific exam**

Download a specific exam with it's respective tasks.

*Route:*
```
GET /auth/exams/grade/examnumber/
```
*Request body:*
```
{
	"test-tasks": []
}
//or none
```
*Response body:*
```
{
	"test-tasks": [
		{ "task": "1"},
		{ "task": "2"},
		{ "task": "3"},
		{ "task": "4"},
		{ "task": "5"},
	]
}
```

----------


 - **Obtain solutions**

Obtains a package of tasks with their solutions.
*Route:*
```
GET /auth/exams/grade/examnumber/solution/
```
*Request body:*
```
{
	"test-solutions": []
}
//or none
```
*Response body:*
```
{
	"test-solutions": [
		{ "solution": sltnconOne },
		{ "solution": sltnconTwo },
		{ "solution": sltnconThree },
		{ "solution": sltnconFour },
		{ "solution": sltnconFive },
	]
}
```

----------

 - **Obtain solution from a specific task**
Obtains a specific solution with 'n' being the identificator for the task.
*Route:*
```
GET /auth/exams/grade/examnumber/solution/
```
*Request body:*
```
{
	"task": n
}
```
*Response body:*
```
{
	"solution": sltncon
}
```
##Result Handling
Handling with user-created results.
*Route:*
```
POST /auth/exams/result/
```
NOTE: Mysql related actions will draw files from this directory
*Request body:*
```
{
	"token": tkn, 
	"packange-content": packcon, 
	"grade": grade, 
	"exam-number": examnumberinfo, 
	"task": n, 
}
```
*Response body:*
```
{
	"correct-points": points
}
```

##Evaluator algorithm
*placeholder*


